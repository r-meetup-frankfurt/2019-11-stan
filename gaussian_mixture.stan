
data {
  int<lower=1> K;          // number of mixture components
  int<lower=0> N;          // number of data points
  int<lower=0> M;         // number of generated data points
  real y[N];               // observations
  int<lower = 0, upper = 1> run_estimation; // a switch to evaluate the likelihood
  int<lower = 0, upper = 1> run_generation; // a switch to evaluate the likelihood
  
  real beta;
  real mu_0;
  real lambda;
  real nu;
  real sigma_0;
}
transformed data {
  vector[K] beta_K = rep_vector(beta, K);
}
parameters {
  simplex[K] theta;          // mixing proportions
  ordered[K] mu;             // locations of mixture components
  vector<lower=0>[K] sigma;  // scales of mixture components
  
}

model {
  vector[K] log_theta = log(theta);  // cache log calculation
  
  theta ~ dirichlet(beta_K);
  sigma ~ inv_gamma(nu, sigma_0);
  mu ~ normal(mu_0, lambda*sigma);
  
  if(run_estimation==1){
  for (n in 1:N) {
  vector[K] lps = log_theta; 
  for (k in 1:K)
  lps[k] += normal_lpdf(y[n] | mu[k], sigma[k]);
  target += log_sum_exp(lps);
  }
}
}
generated quantities {


  vector[M] y_sim;
  int x[M];

  for(i in 1:M) {
    x[i] = categorical_rng(theta);
    y_sim[i] = normal_rng(mu[x[i]], sigma[x[i]]);
  }

}

