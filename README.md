## RStan: Bayesian Inference mit MonteCarlo Simulation in R

Einführung in die statistische Modellierung und Inferenz mittels Hamiltonian Monte Carlo (HMC) mit Stan in R.

Dazu werden wir die Grundzüge der Bayesschen Inferenz durch Monte Carlo Simulation erläutern und die Unterschiede zwischen MCMC und HMC darstellen.
Danach betrachten wir die Umsetzung von ein paar einfachen Modellen und diskutieren anhand der relevanten Analyse-Tools die Ergebnisse und Fallstricke.

Der Vortrag dauert etwa eine Stunde; im Anschluss ist Zeit für Networking und Diskussionen.

Im Vortrag werde ich mit Folien durch das Themen führen. Wenn ihr die Beispiele direkt vor Ort ausprobieren wollt bringt euren Laptop mit. Benötigte Software:

- RStudio
- C++ Toolchain (siehe https://github.com/stan-dev/rstan/wiki/RStan-Getting-Started)
- Pakete: "rstan", "rstanarm", "shinystan", "bayesplot", "dplyr", "tidyr", "ggplot", "rmarkdown"
