---
title: 'RStan: Bayesian Inference mit MonteCarlo Simulation in R'
author: "Daniel Krieg"
date: 24.09.2019
output:
  html_document: 
    css: "media/style_document.css"
    toc: yes
  html_notebook: default
  ioslides_presentation:
    logo: "media/logo.jpg"
    css: "media/style.css"
    smaller: true
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE,
                      eval=TRUE,
                      cache=TRUE, 
                      warning=FALSE,
                      message=FALSE)
htmltools::tagList(rmarkdown::html_dependency_font_awesome())

library(rstan)
library(rstanarm)
library(bayesplot)
library(dplyr)
library(ggplot2)


rstan_options(auto_write = TRUE)
options(mc.cores = parallel::detectCores())

### colors

ADV_green <- rgb(0, 111, 78, maxColorValue = 255)
ADV_red <- rgb(153, 27, 86, maxColorValue = 255)

```

```{r gen_data}
N <- c(A=100,B=300)

mean <- 2
variance <- 1

set.seed(100)
bird_data <- bind_rows(tibble(bird="A", freq_delta=rnorm(N["A"], -mean, variance)),
                       tibble(bird="B", freq_delta=rnorm(N["B"], mean, variance))) %>%
  mutate(category=factor(bird))

```


```{r setup_models}

bird_gmm <- rstan::stan_model("gaussian_mixture.stan",
                              "bird_gaussian_mixture")

bird_gmm_non_ordered <- rstan::stan_model("gaussian_mixture_non_ordered.stan",
                                          "bird_gaussian_mixture_non_ordered")
```



***

__AGENDA__

* Willkommen
* Statistische Modellierung 
* Inferenz 
* Stan
* Zusammenfassung


<div class="notes">

Struktur:

  - Willkommen
  - Vorstellung: Über mich und ADVISORI
  - Organisatorisches (Essen, Getränke, Gast-WLAN)
  - Meetup: Recap+Planung, Erwartungshaltung (-> nach dem Vortrag)
  - Vortragsmaterial: Notebook zum Ausführen
  - Exkurs: Markdown, Notebook, ioslides
  - Vortrag
  - Networking
</div>

Statistische Modellierung
====================

Statistische Modellierung & Inferenz
------------

Wir unterstellen einen stochastischen Zusammenhang zwischen verschiedenen Variablen. Ziel ist die Ermittlung von unbeobachteten Parametern. Das heißt, gegeben die Daten finde

- den wahrscheinlichsten Parameterwert: MAP-Schätzer (maximum-a-posterior)
- die Wahrscheinlichkeitsverteilung des Parameters: Posterior-Dichte

Bayesian Modeling
----------------

Ein Bayessches Modell kann man aus zwei Richtungen betrachten:

1. Top-Down: Generierung von möglichen Beobachtungen beginnend bei den Parametern bzw. Hyperparametern
1. Bottom-Up: Ermittlung / Inferenz der Parameterverteilungen beginnend bei den tatsächlichen Beobachtungen 


```{r, out.width = "300px"}
knitr::include_graphics("media/Generation_Inference.png")
```

Der Posterior $P(\Theta | D)$ ist proportional zum Likelihood $P(D | \Theta)$ multipliziert mit dem Prior $P(\Theta)$.


<div class="notes">
- Die Hyperparameter können entweder fixe Werte sein, oder selbst wieder einer Verteilung folgen
- Conjugate Prior: Die Struktur / Familie des Posterior ist identisch zum Likelihood --> Analytisch einfach. 

</div>



***

### Beispiel:
<div class="" style="width:90%">
  Vogelart A
  <i class="fas fa-dove fa-3x" style="width:20%;text-align:center;color:rgb(0, 111, 78);"></i>
  <i class="fas fa-blind fa-3x" style="width:20%;text-align:center;color:grey;"></i>
  <i class="fas fa-crow fa-flip-horizontal fa-3x" style="width:20%;text-align:center;color:rgb(153, 27, 86);"></i>
  Vogelart B
</div>
<br>
Das Zwitschern von zwei Vogelarten unterscheiden sich nur leicht in der Frequenz. Welche Vogelart hat man gehört? 

--> Was ist die Wahrscheinlichkeit, dass es sich um die Vogelart $V$ handelt, wenn ich die Frequenz $f$ gehört habe:
$$P(V|f)$$
    
***

### Daten




```{r echo=FALSE}

bird_data %>%
  rename(Vogelart = bird) %>%
  group_by(Vogelart) %>% 
  summarise(Beobachtungen=n(), Mittelwert=mean(freq_delta), Standardabweichung=sd(freq_delta)) %>%
  mutate(Anteil=Beobachtungen/sum(Beobachtungen)) %>%
  knitr::kable()


```

```{r eval=TRUE, echo=FALSE, fig.height=3, warning=FALSE}


bird_data %>%
  ggplot() +
  geom_density(aes(x=freq_delta, y=(..count../nrow(bird_data)), fill=bird), size=1) +
  scale_fill_manual(name="Vogelart", 
                     values = c("A"=alpha(ADV_green, 0.3),
                                "B"=alpha(ADV_red, 0.3))) +
  scale_x_continuous(name="Frequenz (Abweichung)") +
  scale_y_continuous(name="Dichte", label=scales::percent)

```

    
    
Supervised Learning
------------------

Unser Trainingsdatenset enthält sowohl die Beobachtung (Frequenz) als auch die relevante Zielgröße (Vogelart).

Dann können wir einen Klassifizier trainieren (z.B. durch eine logistische Regression).

Vorteil: keine Verteilungsannahmen notwendig.

Nachteil: nicht flexibel erweiterbar; generalisiert ggf. schlecht. 

```{r fit_model_glm}
bird_glm_fit <- rstanarm::stan_glm(category~freq_delta, family = binomial(), 
                                   data=bird_data,
                                   seed=100)
```



```{r}
bird_glm_predict <- bird_data %>% 
  bind_cols(as.data.frame(t(posterior_predict(bird_glm_fit, 
                                              draws=50, seed=200)))) %>% 
  tidyr::gather("key", "value", starts_with("V"))


```

***

```{r}
plot(bird_glm_fit)
```

<div class="notes">
- Die Koeffizienten der logisitschen Regression
- Im Gegensatz zur üblichen Maximum-Likelihood Regression fließt hier der Prior ein und wir erhalten eine Posterior-Verteilung der Parameter.
</div>

***

```{r}
bird_glm_predict %>%
  mutate(group=paste0(key, value),
         bird = factor(value, labels = c("A", "B"))) %>%
  ggplot(aes(x=freq_delta,group=group, color=bird)) + 
  geom_density(aes(y=(..count../nrow(bird_data)), color=bird)) +
  geom_density(data=bird_data, inherit.aes = FALSE, aes(x=freq_delta, y=(..count../nrow(bird_data)), fill=bird), color="black", size=1) +
  geom_vline(xintercept = -bird_glm_fit$coefficients["(Intercept)"]/bird_glm_fit$coefficients["freq_delta"]) +
  scale_color_manual(name="Vogelart", 
                     values = c("A"=alpha(ADV_green, 0.3),
                                "B"=alpha(ADV_red, 0.3))) +
  scale_fill_manual(name="Vogelart", 
                     values = c("A"=alpha(ADV_green, 0.3),
                                "B"=alpha(ADV_red, 0.3))) +
  scale_x_continuous(name="Frequenz (Abweichung)") +
  scale_y_continuous(name="Dichte", label=scales::percent)

```


<div class="notes">
- Die Linie stellt die Entscheidungsgrenze dar.
- Mihilfe der Posterior Verteilung der Parameter können wir nun Prognosen durchführen und erhalten die Posterior Predictive Distributions

- Übergang: Was ist wenn ich nur die Beobachtung aber keine Info über die Vogelart habe?
</div>


Unsupervised Learning
-----------


```{r fit_model_gmm}
M <- 100

bird_gmm_samples <- sampling(bird_gmm, 
                        warmup=500, iter=1000, chains=4,
                        control=list(adapt_delta = 0.9),
                        data=list(K=2, 
                                  M=M,
                                  N=nrow(bird_data), 
                                  y=bird_data$freq_delta,
                                  beta=1,
                                  mu_0=0,
                                  lambda=1,
                                  nu = 1,
                                  sigma_0 = 1,
                                  run_estimation = 1, run_generation=1),
                        seed = 100)

```


Wir trainieren ein generatives Modell auf Basis der reinen Beobachtung, indem wir ein stochastisch kausalen Zusammenhang modellieren:

![Gaussian Mixture Model](media/Bayesian-gaussian-mixture.svg)

(By Benwing - Created using LaTeX, TikZ, CC BY 3.0, https://commons.wikimedia.org/w/index.php?curid=18934191)

***
#### Posterior Parameters

```{r}
plot(bird_gmm_samples, pars=c("theta", "mu", "sigma"))
```

***

#### Posterior Predictive Samples

```{r}


bird_gmm_predict <- bird_gmm_samples %>% 
  as.data.frame() %>%
  as.tbl() %>%
  select(starts_with("y_sim"), starts_with("x")) %>%
  mutate(iter = seq(1:n())) %>%
  tidyr::gather("var", "value", -iter) %>%
  tidyr::extract("var", c("var", "count"), regex="(.+)\\[([[:alnum:]]+)") %>%
  mutate(count = as.integer(count)) %>%
  tidyr::spread("var", "value") %>%
  arrange(iter, count)


bird_gmm_predict %>%
     mutate(category = factor(x, labels = c("A", "B")),
            group = paste0(category, iter)) %>%
  filter(iter%%20 == 0) %>%
  ggplot(aes(x=y_sim, group=group)) +
  geom_density(aes(y=(..count../M), color=category), linetype=1, trim=FALSE)+
  geom_density(data=bird_data, 
               aes(x=freq_delta, y=(..count../nrow(bird_data)), fill=category), size=1, inherit.aes = FALSE) + 
  scale_color_manual(name="Vogelart", 
                     values = c("A"=alpha(ADV_green, 0.3),
                                "B"=alpha(ADV_red, 0.3))) +
  scale_fill_manual(name="Vogelart", 
                     values = c("A"=alpha(ADV_green, 0.3),
                                "B"=alpha(ADV_red, 0.3))) +
  scale_x_continuous(name="Frequenz (Abweichung)") +
  scale_y_continuous(name="Dichte", label=scales::percent)

```



Inferenz
============


MonteCarlo Simulation
--------

```{r, out.width = "150px"}
knitr::include_graphics("media/Generation_Inference.png")
```

- Die Formulierung der Modelle ist Top-Down, so dass man schrittweise erst Hyperparameter, dann Parameter, dann Beobachtungen samplen kann.
- Inferenz erfordert die Umkehrung der probabilistischen Zusammenhänge
    - analytisch: meist schwer bis unmöglich
    - numerisch: sehr aufwendig in hochdimensionalen Modellen
    
Lösung: 

1. Definiere eine Markov Kette deren Gleichgewichtsverteilung dem Posterior entspricht
2. Erzeuge Samples Samples mittels Monte Carlo Simulation
3. Die empirische Sample-Verteilung konvergiert gegen den Posterior 

<div class="notes">
- numerisch: Berechnung der Normierungskonstante erfordert numerische Integration über den kompletten Raum der Parameter und Hyperparameter
- Frage: Was ist eine Markov Chain? Stochastischer Prozess bei dem die Übergänge nur vom aktuellen Zustand abhängen.


</div>

***

### Demo

Markov Chains: Why Walk When You Can Flow?

--> http://elevanth.org/blog/2017/11/28/build-a-better-markov-chain/

<div class="notes">
- MCMC: Random Walk
    - Problematisch bei komplexer Geometrie z.B. korrelierte Paramter: Wie soll die Sprungweite (global) eingestellt werden?  --> viele Rejections
- HMC: Random flow
    - Schubsen + stoppen (Schwarze Punkte sind keine Sample)
    - Nutzt die Informationen über lokale Geometrie
    - Problematisch: Wie lange soll die Bewegung dauern? U-Turn möglich, dann wieder hoch korrelierte Sample --> NUTS
    - Theoretisch keine Rejection, da Hamiltionen konstant. kommen aber aufgrund der numerischen Integration vor --> Tradeoff Rejection-Rate vs. Anzahl numerischer Integrationsschritte
</div>


*** 

### Markov Chain Monte Carlo (MCMC)
"Random Walk entlang einer Markov Kette im Parameterraum"

*Vorgehen*:
Wähle eine zufällige Startposition des Sampling-Partikels. Die Partikelenergie entspricht dem Likelihood.

1. Berechne die Energie der aktuellen Partikelposition $\Theta_{i}$
2. Suche zufällig neue Position $\Theta_{i+1}$ in der Nähe aus und berechne die Energie
3. Akzeptiere die neue Position als nächstes Sample mit der Wahrscheinlichkeit $min(1, E(\Theta_{i+1})/E(\Theta_{i}))$ oder verwerfe das Sample (Rejection)
4. Wiederhole

*Vorteil*: Einfach zu implementieren

*Nachteil*: Ineffizient (viele Rejections), hohe Korrelation der benachbarten Samples

<div class="notes">
- Software-Implementierung: BUGS bzw. JAGS (Link im Anhang)

</div>

***

### Hamiltonian Monte Carlo (HMC)
"Random Slide entlang einer Markov Kette im Parameterraum"

*Vorgehen*:
Wähle eine zufällige Startposition des Sampling-Partikels. Die Partikelenergie entspricht dem Likelihood (Potentiell) und der Geschwindigkeit (Kinetisch).

1. Schubse das Partikel an der aktuellen Partikelposition $\Theta_{i}$ in eine zufällig Richtung und berechne sein Energie
2. Lass das Partikel für eine feste Zeit (Anzahl Schritte) durch den Parameterraum entsprechend der Posterior-Geometrie gleiten, stoppe es an der Stelle $\Theta_{i+1}$ und berechne seine Energie
3. Akzeptiere die neue Position als nächstes Sample mit der Wahrscheinlichkeit $min(1, E(\Theta_{i+1})/E(\Theta_{i}))$ oder verwerfe das Sample (Rejection)
4. Wiederhole

*Vorteil*: Effizient (wenige Rejections), Geringe Korrelation 

*Nachteil*: Aufwändiger zu implementieren. Viele Berechnungen pro Sample für Bewegungsgleichung


Parameterwahl
---------

Beide Varianten (MCMC und HMC) haben Simulationsparameter die

a. durch den Algorithmus an die Geometrie angepasst werden:

    - MCMC: Schrittweite des Random Walk
    - HMC: Laufzeit des Partikels
b. durch den Nutzer gewählt werden können oder müssen:

    - Warmup-Iterationen für automatische Anpassung (siehe oben)
    - HMC: Maximale Rejection-Quote

Das Tuning der Parameter hängt vom Modell und den Daten ab und erfordert teilweise Erfahrung.
Die Warnmeldungen von Stan sind dabei sehr ausführlich und können bei der Optimierung helfen. 


Chains
-----

Die Exploration in komplexen Geometrien wie

- multimodale Posterior
- sehr hochdimensionale Modelle

erfordert u.a. das wiederholte Sampling mit verschiedenen Anfangsbedingungen (Chains). Diese können verschiedene Bereiche des Posterior erkunden.

***

### Beispiel: Non-identifiability

Ein Modell ist nicht "identizierbar" wenn verschiedene Ausprägungen eines Parameters zum gleichen Posterior führen. 

In unserem Vogel-Beispiel ist nicht explizit spezifiert, 

- welcher Vogel (A oder B)
- welche Komponente (negativer oder positiver Mittelwert) darstellt.

Allerdings wurden die Komponenten eingeschränkt, dass sie __geordnet__ ($\mu_1 <= \mu_2$) sein müssen. Ohne diese Einschränkung ist das Modell nicht identifizierbar und der Posterior multimodal.

***

Das Problem mit der Multimodalität zeigt sich

- in den Warnmeldungen:


```{r fit_model_gmm_multimodal, warning=TRUE}

bird_multimodal_samples <- sampling(bird_gmm_non_ordered, 
                        warmup=500, iter=1000, chains=10,
                        control=list(adapt_delta = 0.8),
                        data=list(K=2, 
                                  M=M,
                                  N=nrow(bird_data), 
                                  y=bird_data$freq_delta,
                                  beta=1,
                                  mu_0=0,
                                  lambda=1,
                                  nu = 1,
                                  sigma_0 = 1,
                                  run_estimation = 1, run_generation=1),
                        seed = 100)

```

***

- und an dem breiten Posterior der Mittelwerte $\mu$:

```{r}
plot(bird_multimodal_samples, pars=c("theta", "mu", "sigma"))
```

***

Die Analyse der verschiedenen Chains zeigt, dass diese aus verschiedene Modes samplen.

--> Öffne Modell in ShinyStan

```{r eval=FALSE, echo=TRUE}
launch_shinystan(bird_multimodal_samples)
```


Stan
===============

Funktionalität
------------

Stan ist eine Plattform für statistische Modellierung und Inferenz:

Der Benutzer spezifiert die Log-Dichteverteilung in der Statistik-Sprache von Stan und erhält:

- full Bayesian statistical inference with MCMC sampling (NUTS, HMC)
- approximate Bayesian inference with variational inference (ADVI)
- penalized maximum likelihood estimation with optimization (L-BFGS)

Der Stan-Code wird in ein C++ Template überführt und kompiliert. Das Programm erzeugt dann die Samples.

R-Pakete
----------

- `rstan` bildet das Interface von R zu Stan
    - Kompiliere den Stan-Code
    - Führe das Sampling durch
    - Extrahiere Ergebnisse
- `rstanarm` stellt fertige Modelle für Standard-Regressionsaufgaben zur Verfügung
    - Führe eine generalisierte lineare Regression mit `rstanarm::stan_glm` durch
- `bayesplot` erzeugt Analysegrafiken mit `ggplot`
- `shinystan` generiert ein interaktives Dashboard mit allen relevanten Analysefunktionen


Modellierung in Stan
-------------

Die Dokumentation umfasst:

- Stan User's Guide: Programmiertechniken und Beispielmodelle
- Stan Language Reference Manual: Beschreibung der Programmiersprache für die Modelle, die Inferenzalgorithmen sowie Prognose- und Analysetools
- Stan Language Functions Reference: Referenz aller mathematischen Funktionen und Verteilungen in Stan

***

### Struktur des Modellcodes

Ein Modell enthält folgende Abschnitte:

- _data_: Die Inputdaten, welche beim Sampling übergeben werden
- _transformed data_ (optional): Transformation von Inputdaten
- _parameters_: Die Modellparameter, welche über einen Prior Teil der Inferenz sind
- _transformed parameters_ (optional): Transformation der Parameter
- _model_: Die Verteilung und Abhängigkeiten (Prior, Likelihood)
- _generated quantities_ (optional): Berechnete Größen auf Basis der Parametersamples

***

### Beispiel

Eine normalverteilte Zufallsvariable ohne Beobachtungen und Parameter-Prior

```{stan echo=TRUE, output.var="example_normal"}
data {
  real mu;
  real sigma;
}
parameters {
  real y;
}
model {
  y ~ normal(mu, sigma);
}
```

***

```{r fit_example_normal, echo=TRUE}
example_normal_sample <- rstan::sampling(example_normal, iter=100, data=list(mu=1, sigma=1))
print(example_normal_sample)
```

***


### Beispiel

Eine normalverteilte Zufallsvariable mit Beobachtungen und mu-Prior und fixer Varianz

```{stan echo=TRUE, output.var="example_normal_prior"}
data {
  int<lower=0> N;          // number of data points
  real y[N];
  real sigma;
}
parameters {
  real mu;
}
model {
  mu ~ normal(0,1);
  y ~ normal(mu, sigma);
}
```

***

```{r fit_example_normal_prior, echo=TRUE}
set.seed(100)
y <- rnorm(100, 1, 1)
example_normal_prior_sample <- rstan::sampling(example_normal_prior, iter=100,
                                               data=list(y=y, N=length(y), sigma=1))
print(example_normal_prior_sample)
print(mean(y))
```

***

### Gaussian Mixture Model


```

data {
  int<lower=1> K;          // number of mixture components
  int<lower=0> N;          // number of data points
  int<lower=0> M;         // number of generated data points
  real y[N];               // observations
  int<lower = 0, upper = 1> run_estimation; // a switch to evaluate the likelihood
  int<lower = 0, upper = 1> run_generation; // a switch to evaluate the likelihood
  
  real beta;
  real mu_0;
  real lambda;
  real nu;
  real sigma_0;
}

```

***


```
transformed data {
  vector[K] beta_K = rep_vector(beta, K);
}
parameters {
  simplex[K] theta;          // mixing proportions
  ordered[K] mu;             // locations of mixture components
  vector<lower=0>[K] sigma;  // scales of mixture components
  
}

```

***


```
model {
  vector[K] log_theta = log(theta);  // cache log calculation
  
  theta ~ dirichlet(beta_K);
  sigma ~ inv_gamma(nu, sigma_0);
  mu ~ normal(mu_0, lambda*sigma);
  
  if(run_estimation==1){
    for (n in 1:N) {
      vector[K] lps = log_theta; 
      for (k in 1:K)
        lps[k] += normal_lpdf(y[n] | mu[k], sigma[k]);
        target += log_sum_exp(lps);
      }
    }
  }
```

***


```
generated quantities {


  vector[M] y_sim;
  int x[M];

  for(i in 1:M) {
    x[i] = categorical_rng(theta);
    y_sim[i] = normal_rng(mu[x[i]], sigma[x[i]]);
  }

}

```

Fallstricke
----------------

Probleme bei komplizierter Posterior-Geometrie: Der Sampler wird sehr langsam oder erkundet nicht den  ganzen Parameterraum. 

### Beispiel: Neals Funnel

```
parameters {
  real y;
  vector[9] x;
}
model {
  y ~ normal(0, 3);
  x ~ normal(0, exp(y/2));
}
```

***

```{r, out.width = "500px"}
knitr::include_graphics("https://mc-stan.org/docs/2_20/stan-users-guide/img/funnel.png")
```

<div class="notes">
- Divergenzen aufgrund der Geometrie: Globale Explorationsparameter funktionieren nicht im Funnel
- Divergenzen werden angezeigt und können diagnostiziert werden.
</div>

***

Reparameterisierung löst das Sampling-Problem:

```
parameters {
  real y_raw;
  vector[9] x_raw;
}
transformed parameters {
  real y;
  vector[9] x;

  y = 3.0 * y_raw;
  x = exp(y/2) * x_raw;
}
model {
  y_raw ~ std_normal(); // implies y ~ normal(0, 3)
  x_raw ~ std_normal(); // implies x ~ normal(0, exp(y/2))
}
```


Zusammenfassung
========

Durchführung
------------

- Modell formulieren
    - Variablen und Verteilungen definieren
    - Hilfreich: Grafische Notation zur Darstellung von Abhängigkeiten
- Bei einem Standardregressionsmodell --> `rstanarm`-Funktionen verwenden
- Bei komplexeren Modellen --> in Stan-Modelldefinition überführen
    - .stan-Datei erstellen
    - Modell kompilieren: `rstan::stan_model`
    - Sample erzeugen: `rstan::sampling`
    
***
    
- Modell prüfen
    - Sampling ohne Beobachtungen durchführen (Prior Predictive Samples) 
    - Künstliche Daten erzeugen und Modellschätzung durchführen
- Ergebnisse überprüfen
    - Warning-Messages beachten
    - Ergebnisse ausgeben (`print`)
    - Analysefunktionen nutzen --> Dashboard mit `shinystan`


Links {.smaller}
----------

- [Stan Homepage](https://mc-stan.org/)
- [JAGS Homepage](http://mcmc-jags.sourceforge.net/)
- Einführung in MCMC und HMC:
    - http://arogozhnikov.github.io/2016/12/19/markov_chain_monte_carlo.html
    - http://elevanth.org/blog/2017/11/28/build-a-better-markov-chain/
- Beispiele und Case-Studies:
    - [Mixture Models](https://betanalpha.github.io/assets/case_studies/identifying_mixture_models.html)
    - [Gaussian Processes](https://betanalpha.github.io/assets/case_studies/gp_part1/part1.html)
- Divergenz & Reparametrisierung: 
    - [Brief Guide to Stan’s Warnings](https://mc-stan.org/misc/warnings.html)
    - [Stan User Guide: Reparameterization](https://mc-stan.org/docs/2_20/stan-users-guide/reparameterization-section.html)
    - [User Case Study: Divergences and Bias](https://mc-stan.org/users/documentation/case-studies/divergences_and_bias.html)
    - [Blog: Taming Divergences](https://www.martinmodrak.cz/2018/02/19/taming-divergences-in-stan-models/)
    
